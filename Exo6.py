def gen_generique( a, c, m, xn):
    return ( a*xn + c) % m


x1 = genr_generique(12, 0, 101, 11)
for i in range(0,10):
    print(x1)
    x1 = gen_generique(12, 0, 101, x1)
    

file1 = open("fichier2d.txt", "w")
x1 = gen_generique(12, 0, 101, 12345)

for i in range(0,100):
    x2 = gen_generique(12, 0, 101, x1)
    file1.write(str(round(x1/101.0, 5))+","+str(round(x2/101.0 ,5))+"\n")
    x1=x2
file1.close()


file2 = open("fichier3d.txt", "w")
x1 = gen_generique(12, 0, 101, 12345)

for i in range(0,100000):
    x2 = gen_generique(12, 0, 101, x1)
    x3 = gen_generique(12, 0, 101, x2)
    fichier2.write("["+str(round(x1/101.0, 5))+","+str(round(x2/101.0 ,5))+","+str(round(x3/101.0 ,5))+"],\n")
    x1=x2
file2.close()
